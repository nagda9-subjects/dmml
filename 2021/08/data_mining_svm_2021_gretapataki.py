import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.datasets import make_blobs


def plot_data(X, y):
    plt.scatter(X[:, 0], X[:, 1], c=y, s=40, cmap=plt.cm.Paired)
    plt.show()


# generate and plot data

X = np.array([(1, 1), (1, 2), (2, 1), (2, 2), (2, 6), (4, 6), (5, 5), (5, 3)])
y = [0, 0, 0, 0, 1, 1, 1, 1]

plt.grid()
plot_data(X, y)


# fit svm model

def fit_svm(X, y):
    clf = svm.SVC(kernel='linear')
    clf.fit(X, y)
    return clf


# print support vectors

def print_svm_data(clf):
    # Support vectors
    sv = clf.support_vectors_
    print(f'Support vectors: \n {sv}')

    # Number of support vectors for each class.
    print(f'Number of support vectors: {clf.n_support_}')

    w = clf.coef_
    b = clf.intercept_

    print(f'w: {w}')
    print(f'b: {b}')


clf = fit_svm(X, y)
print_svm_data(clf)


def plot_svm(X, y, sv, grid=False, title='SVM'):
    # visualize data, support vectors, decision boundary

    # plot data points
    plt.scatter(X[:, 0], X[:, 1], c=y, s=40, cmap=plt.cm.Paired)

    # mark support vectors
    plt.scatter(sv[:, 0], sv[:, 1], s=100,
                linewidth=1, facecolors='none', edgecolors='k')

    # plot decision boundary/maximum margin hyperplane and margins
    #  (xlim, ylim, xx, yy... refer to the axes, not the input and output of the classifier)
    # get axes
    ax = plt.gca()
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    #  create grid to evaluate model
    xx = np.linspace(xlim[0], xlim[1], 30)
    yy = np.linspace(ylim[0], ylim[1], 30)
    YY, XX = np.meshgrid(yy, xx)
    xy = np.vstack([XX.ravel(), YY.ravel()]).T
    # run classifier for all points in the mesh
    Z = clf.decision_function(xy).reshape(XX.shape)
    # plot decision boundary and margins
    ax.contour(XX, YY, Z, colors='k', levels=[-1, 0, 1], alpha=0.5,
               linestyles=['--', '-', '--'])

    if grid:
        plt.grid()

    plt.title(title)
    plt.show()


sv = clf.support_vectors_
plot_svm(X, y, sv, grid=True)

# generate and plot data
# help(make_blobs)
X, y = make_blobs(n_samples=100, centers=2, random_state=0, cluster_std=0.60)

plot_data(X, y)

clf = fit_svm(X, y)
print_svm_data(clf)

sv = clf.support_vectors_
plot_svm(X, y, sv, grid=True)

"""Regularization"""

X, y = make_blobs(n_samples=100, centers=2, random_state=0, cluster_std=0.90)


# fit svm model
# with strong regularisation (wide margin, even if misclassifications)
# test different C values!

def fit_svm_regularisation(X, y, C=1):
    clf = svm.SVC(kernel='linear', C=C)
    clf.fit(X, y)
    return clf


clf = fit_svm_regularisation(X, y)
print_svm_data(clf)
sv = clf.support_vectors_
plot_svm(X, y, sv, grid=True)

for c in [0.01, 0.1, 1, 10, 100, 1000]:
    clf = fit_svm_regularisation(X, y, c)
    sv = clf.support_vectors_
    plot_svm(X, y, sv, grid=True, title=f'SVM: c={c}')

"""Nonlinear"""

# generate and plot data
np.random.seed(0)
X = np.random.randn(300, 2)
y = np.logical_xor(X[:, 0] > 0, X[:, 1] > 0)


plt.scatter(X[:,0], X[:,1], c=y, s=40, cmap=plt.cm.Paired)
plt.show()

# fit svm model
def fit_non_linear(kernel='rbf', gamma=1):
    clf = svm.SVC(kernel=kernel, gamma=gamma)
    clf.fit(X, y)
    return clf


clf = fit_non_linear()


# print_svm_data(clf)

def print_svm_data_nonlinear(clf):
    # Support vectors
    sv = clf.support_vectors_
    print(f'Support vectors: \n {sv}')

    # Number of support vectors for each class.
    print(f'Number of support vectors: {clf.n_support_}')
    return sv


sv = print_svm_data_nonlinear(clf)

plot_svm(X, y, sv)

for gamma in [0.001, 0.01, 0.1, 1, 5]:
    clf = fit_non_linear(gamma=gamma)
    plot_svm(X, y, sv, title=f'SVM: gamma={gamma}')

for kernel in ['linear', 'poly', 'rbf', 'sigmoid']:
    clf = fit_non_linear(kernel=kernel)
    plot_svm(X, y, sv)
