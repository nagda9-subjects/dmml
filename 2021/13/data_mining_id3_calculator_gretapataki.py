import math


def log(number, base):
    if number == 0:
        return 0
    else:
        return math.log(number, base)


# get the number of all yes cases for the dataset
def get_yes_count(dataset):
    return sum([attr[0] for attr in dataset])


# get the number of all no cases for the dataset
def get_no_count(dataset):
    return sum([attr[1] for attr in dataset])


# get the number of cases for the dataset
def get_all_count(dataset):
    yes_count = get_yes_count(dataset)
    no_count = get_no_count(dataset)
    return yes_count + no_count


# calculate the information before split
def calc_before_split(dataset):
    yes_count = get_yes_count(dataset)
    no_count = get_no_count(dataset)
    all_count = yes_count + no_count
    yes_prob = yes_count / all_count
    no_prob = no_count / all_count
    return (-yes_prob * log(yes_prob, 2) - no_prob * log(no_prob, 2))


# calculate the information after split for one branch of an attribute (one possible value, eg. red)
def calc_after_split_for_branch(dataset, branch):
    yes_count = dataset[branch - 1][0]
    no_count = dataset[branch - 1][1]
    all_count = yes_count + no_count
    yes_prob = yes_count / all_count
    no_prob = no_count / all_count
    return (-yes_prob * log(yes_prob, 2) - no_prob * log(no_prob, 2))


# calculate the information after split for an attribute
def calc_after_split(dataset):
    after_split = 0
    all_count = get_all_count(dataset)
    for i in range(1, len(dataset) + 1):
        after_split_for_branch = calc_after_split_for_branch(dataset, i)
        branch_count = sum(dataset[i - 1])
        after_split += (branch_count / all_count) * after_split_for_branch

    return after_split


# calculate the information gain
def calc_info_gain(dataset):
    before_split = calc_before_split(dataset)
    after_split = calc_after_split(dataset)

    return before_split - after_split


def calculate_example(dataset):
    before_split = calc_before_split(dataset)
    print('Information before split', '\t\t', round(before_split, 2))

    number_of_branches = len(dataset)
    for i in range(1, number_of_branches + 1):
        after_split_for_branch = calc_after_split_for_branch(dataset, i)
        print(f'Information after split, branch{i}', '\t', round(after_split_for_branch, 2))

    after_split = calc_after_split(dataset)
    print('Information after split', '\t\t', round(after_split, 2))

    info_gain = calc_info_gain(dataset)
    print('Information gain', '\t\t\t', round(info_gain, 2))


# Example1 - Background color
# number of yes and no samples (columns) for each possible value of the attribute (row)

dataset = [
    [4, 1],  # red, yes: 4, no: 1
    [4, 2],  # green, yes: 4, no: 2
    [2, 5]  # blue, yes: 2, no: 5
]

calculate_example(dataset)

# Example1 - Position

dataset = [
    [2, 3],  # left, yes: 2, right: 3
    [4, 3],  # center, yes: 4, right: 3
    [4, 2],  # right, yes: 4, right: 2
]

calculate_example(dataset)
